import React, { Component } from 'react'
import PropTypes from 'prop-types'

class TodoItem extends Component {

    getStyle = () => {
        return {
            background: '#F4f4f4',
            padding: '10px',
            borderBottom: '1px #ccc dotted',
            textDecoration: this.props.todo.completed ? 'line-through' : 'none'
        }
    }

    render() {
        const { id, title } = this.props.todo
        return (
        <div style={this.getStyle()}>
            <p>
                <input type="checkbox" onChange={ this.props.toggleComplete.bind(this, id) } />{ ' ' }
            { title }
            <button style={btnStyle} onClick={this.props.delTodo.bind(this, id)}><span className="show-for-sr">Remove item { title }{ ' ' }</span>x</button>
            </p>
        </div>
        )
    }
}

// PropTypes
TodoItem.propTypes = {
    todo: PropTypes.object.isRequired,
    markComplete: PropTypes.func.isRequired,
    delTodo: PropTypes.func.isRequired
}

const btnStyle = {
    background: '#f00',
    color: '#fff',
    border: 'none',
    padding: '5px 9px',
    borderRadius: '50%',
    cursor: 'pointer',
    float: 'right'
}

export default TodoItem
